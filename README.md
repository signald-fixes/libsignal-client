# libsignal-client

this repo manages building and publishing of [libsignal-client](https://github.com/SignalApp/libsignal-client). The published artifact is a per-target jar in a maven-compatible repo.