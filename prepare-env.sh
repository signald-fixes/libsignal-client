#!/bin/bash
set -exuo pipefail

apt-get update

PACKAGES="git"
case "${TARGET}" in
    aarch64-unknown-linux-gnu)
        apt-get install -y "${PACKAGES}" {cpp,g++,gcc}-aarch64-linux-gnu
    ;;
    arm-unknown-linux-gnueabi)
        apt-get install -y "${PACKAGES}" {cpp,g++,gcc}-arm-linux-gnueabi
    ;;
    arm-unknown-linux-gnueabihf)
        apt-get install -y "${PACKAGES}" {cpp,g++,gcc}-arm-linux-gnueabihf
    ;;
    armv7-unknown-linux-gnueabihf)
        apt-get install -y "${PACKAGES}" {cpp,g++,gcc}-arm-linux-gnueabihf
    ;;
    *)
        apt-get install -y "${PACKAGES}"
    ;;
esac